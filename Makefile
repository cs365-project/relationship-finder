.PHONY: build
.DEFAULT_GOAL: build
build:
	git submodule init
	git submodule update
	make -C metalanguage-mapper/
	make -C metalanguage-parser/
clean:
	make clean -C metalanguage-mapper/
	make clean -C metalanguage-parser/
